﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static IOnlyNeedToAsk.APIs.WinApi;

namespace IOnlyNeedToAsk.Models {
    class ModifiedKeyboardCharacter : KeyboardCharacter {
        private Modifiers modifier;

        public ModifiedKeyboardCharacter(Keys key, Modifiers modifier) : base(key) {
            this.modifier = modifier;
        }
        public override async Task TypeAsync() {
            await KeyboardEmulator.PressKeyAsync(key, modifier);
        }
    }
}
