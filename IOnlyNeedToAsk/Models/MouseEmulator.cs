﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static IOnlyNeedToAsk.APIs.WinApi;

namespace IOnlyNeedToAsk.Models {
    class MouseEmulator {
        public static void LeftMouseClick(Point<int> p) {
            if (SetCursorPos(p.X, p.Y)) {
                mouse_event((byte)MouseEvents.LEFTDOWN, p.X, p.Y, 0, 0);
                mouse_event((byte)MouseEvents.LEFTUP, p.X, p.Y, 0, 0);
            }
        }

        public static async Task LeftMouseClickAsync(Point<int> p) {
            await Task.Run(() => LeftMouseClick(p));
        }

    }
}
