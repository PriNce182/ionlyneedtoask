﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOnlyNeedToAsk.Models {
    struct Point<T> {
        public T X;
        public T Y;
    }
}
