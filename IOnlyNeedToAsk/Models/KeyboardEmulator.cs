﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static IOnlyNeedToAsk.APIs.WinApi;

namespace IOnlyNeedToAsk.Models {
    class KeyboardEmulator {

        static Random randomizer;

        static Dictionary<char, KeyboardCharacter> Characters = new Dictionary<char, KeyboardCharacter> {
            ['a'] = new KeyboardCharacter(Keys.VK_KEY_A),
            ['b'] = new KeyboardCharacter(Keys.VK_KEY_B),
            ['c'] = new KeyboardCharacter(Keys.VK_KEY_C),
            ['d'] = new KeyboardCharacter(Keys.VK_KEY_D),
            ['e'] = new KeyboardCharacter(Keys.VK_KEY_E),
            ['f'] = new KeyboardCharacter(Keys.VK_KEY_F),
            ['g'] = new KeyboardCharacter(Keys.VK_KEY_G),
            ['h'] = new KeyboardCharacter(Keys.VK_KEY_H),
            ['i'] = new KeyboardCharacter(Keys.VK_KEY_I),
            ['j'] = new KeyboardCharacter(Keys.VK_KEY_J),
            ['k'] = new KeyboardCharacter(Keys.VK_KEY_K),
            ['l'] = new KeyboardCharacter(Keys.VK_KEY_L),
            ['m'] = new KeyboardCharacter(Keys.VK_KEY_M),
            ['n'] = new KeyboardCharacter(Keys.VK_KEY_N),
            ['o'] = new KeyboardCharacter(Keys.VK_KEY_O),
            ['p'] = new KeyboardCharacter(Keys.VK_KEY_P),
            ['q'] = new KeyboardCharacter(Keys.VK_KEY_Q),
            ['r'] = new KeyboardCharacter(Keys.VK_KEY_R),
            ['s'] = new KeyboardCharacter(Keys.VK_KEY_S),
            ['t'] = new KeyboardCharacter(Keys.VK_KEY_T),
            ['u'] = new KeyboardCharacter(Keys.VK_KEY_U),
            ['v'] = new KeyboardCharacter(Keys.VK_KEY_V),
            ['x'] = new KeyboardCharacter(Keys.VK_KEY_X),
            ['y'] = new KeyboardCharacter(Keys.VK_KEY_Y),
            ['z'] = new KeyboardCharacter(Keys.VK_KEY_Z),
            ['A'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_A, Modifiers.VK_LSHIFT),
            ['B'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_B, Modifiers.VK_LSHIFT),
            ['C'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_C, Modifiers.VK_LSHIFT),
            ['D'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_D, Modifiers.VK_LSHIFT),
            ['E'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_E, Modifiers.VK_LSHIFT),
            ['F'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_F, Modifiers.VK_LSHIFT),
            ['G'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_G, Modifiers.VK_LSHIFT),
            ['H'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_H, Modifiers.VK_LSHIFT),
            ['I'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_I, Modifiers.VK_LSHIFT),
            ['J'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_J, Modifiers.VK_LSHIFT),
            ['K'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_K, Modifiers.VK_LSHIFT),
            ['L'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_L, Modifiers.VK_LSHIFT),
            ['M'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_M, Modifiers.VK_LSHIFT),
            ['N'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_N, Modifiers.VK_LSHIFT),
            ['O'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_O, Modifiers.VK_LSHIFT),
            ['P'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_P, Modifiers.VK_LSHIFT),
            ['Q'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_Q, Modifiers.VK_LSHIFT),
            ['R'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_R, Modifiers.VK_LSHIFT),
            ['S'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_S, Modifiers.VK_LSHIFT),
            ['T'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_T, Modifiers.VK_LSHIFT),
            ['U'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_U, Modifiers.VK_LSHIFT),
            ['V'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_V, Modifiers.VK_LSHIFT),
            ['Z'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_X, Modifiers.VK_LSHIFT),
            ['Y'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_Y, Modifiers.VK_LSHIFT),
            ['Z'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_Z, Modifiers.VK_LSHIFT),
            ['0'] = new KeyboardCharacter(Keys.VK_KEY_0),
            ['1'] = new KeyboardCharacter(Keys.VK_KEY_1),
            ['2'] = new KeyboardCharacter(Keys.VK_KEY_2),
            ['3'] = new KeyboardCharacter(Keys.VK_KEY_3),
            ['4'] = new KeyboardCharacter(Keys.VK_KEY_4),
            ['5'] = new KeyboardCharacter(Keys.VK_KEY_5),
            ['6'] = new KeyboardCharacter(Keys.VK_KEY_6),
            ['7'] = new KeyboardCharacter(Keys.VK_KEY_7),
            ['8'] = new KeyboardCharacter(Keys.VK_KEY_8),
            ['9'] = new KeyboardCharacter(Keys.VK_KEY_9),
            ['@'] = new ModifiedKeyboardCharacter(Keys.VK_KEY_2, Modifiers.VK_LSHIFT),
            ['.'] = new KeyboardCharacter(Keys.VK_OEM_PERIOD)
        };

        static KeyboardEmulator() {
            randomizer = new Random();
        }

        public static async Task TypeTextAsync(string text, int minDelay, int maxDelay) {
            for (int i = 0; i < text.Length; i++) {
                char c = text[i];
                var keyboardCharacter = Characters[c];
                await keyboardCharacter.TypeAsync();
                await Task.Delay(randomizer.Next(minDelay, maxDelay));
            }
        }

        public static void PressKey(Keys key, Modifiers modifier) {
            keybd_event((byte)modifier, 0, (byte)KeyboardEvents.KEYDOWN, 0);
            PressKey(key);
            keybd_event((byte)modifier, 0, (byte)KeyboardEvents.KEYUP, 0);
        }

        public static async Task PressKeyAsync(Keys key, Modifiers modifier) {
            await Task.Run(() => PressKey(key, modifier));
        }

        public static void PressKey(Keys key) {
            keybd_event((byte)key, 0, (byte)KeyboardEvents.KEYDOWN, 0);
            keybd_event((byte)key, 0, (byte)KeyboardEvents.KEYUP, 0);
        }

        public static async Task PressKeyAsync(Keys key) {
            await Task.Run(() => PressKey(key));
        }
    }
}
