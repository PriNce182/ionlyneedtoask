﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static IOnlyNeedToAsk.APIs.WinApi;

namespace IOnlyNeedToAsk.Models {
    class KeyboardCharacter {
        protected Keys key;

        public KeyboardCharacter(Keys key) {
            this.key = key;
        }

        public virtual async Task TypeAsync() {
            await KeyboardEmulator.PressKeyAsync(key);
        }
    }
}
