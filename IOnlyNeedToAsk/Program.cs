﻿using IOnlyNeedToAsk.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IOnlyNeedToAsk {
    class Program {
        static async Task Main(string[] args) {

            if (args.Length < 1) {
                Console.WriteLine("Process can not start. File to start is not arranged");
                return;
            } else if (args.Length < 2) {
                Console.WriteLine("Process can not start. Login is not arranged");
                return;
            } else if (args.Length < 3) {
                Console.WriteLine("Process can not start. Password is not arranged");
                return;
            } else if (!File.Exists(args[0])) {
                Console.WriteLine("Process can not start. File to start is not existed");
                return;
            }

            AppDomain.CurrentDomain.UnhandledException += (s, e) => {
                Console.WriteLine($"Failed. Unhandeled exception: {((Exception)e.ExceptionObject).Message}");
            };

            Random randomizer = new Random();
            Console.WriteLine($"{DateTime.Now.ToLongTimeString()} Process started");
            OpenProgram(args[0]);
            await Task.Delay(randomizer.Next(500, 2500));
            Console.WriteLine($"{DateTime.Now.ToLongTimeString()} Simulation started");
            await KeyboardEmulator.TypeTextAsync(args[1], 50, 150);
            await KeyboardEmulator.PressKeyAsync(APIs.WinApi.Keys.VK_TAB);
            await KeyboardEmulator.TypeTextAsync(args[2], 50, 150);
            await KeyboardEmulator.PressKeyAsync(APIs.WinApi.Keys.VK_RETURN);
            await Task.Delay(randomizer.Next(5000, 10000));
            await MouseEmulator.LeftMouseClickAsync(new Point<int> { X = randomizer.Next(850, 920), Y = randomizer.Next(890, 900) });
            await Task.Delay(randomizer.Next(500, 1000));
            await MouseEmulator.LeftMouseClickAsync(new Point<int> { X = randomizer.Next(585, 650), Y = randomizer.Next(265, 280) });
            await Task.Delay(randomizer.Next(500, 1000));
            await KeyboardEmulator.PressKeyAsync(APIs.WinApi.Keys.VK_RETURN);
            Console.WriteLine($"{DateTime.Now.ToLongTimeString()} Simulation ended");
        }

        static void OpenProgram(string filename) {
            var proc = Process.Start(filename);
            proc.WaitForInputIdle();
        }
    }
}
