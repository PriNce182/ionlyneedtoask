# I Only Need To Ask
## About
It is .NET console application. The main aim of the application is auto- start, login and get in queue.
## Install
- Download the **IO**nlyNeedToAsk.exe
- Place .exe file anywhere you want on your PC
- Run application
    - [Download](http://go.microsoft.com/fwlink/?LinkId=863262) & Install .NET Framework 4.7 and higher versions _(If you don't have it)_
    - Start the application
## Example
`/Path/To/IOnlyNeedToAsk.exe "/Path/To/SomeGame.exe" "login" "password"`

## Chanelog
- [05.09.2019] Released version 1.0.0
## Tips
You can use **IO**nlyNeedToAsk in batch with BIOS power-on scheduler and Windows Task Scheduler

Here you find guides for: 

- [BIOS Power-On setup](https://www.gearbest.com/blog/how-to/how-to-automatically-turn-on-the-computer-on-a-schedule-3794) 
- [Windows Task Scheduler](https://www.windowscentral.com/how-create-automated-task-using-task-scheduler-windows-10)